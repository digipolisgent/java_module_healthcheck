package gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.modelmapper.ModelMapper;

import java.util.Map;

/**
 *
 * @author Brecht
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DependencyInfoDto {
    private String description;
    private String result;
    private String details;

    private DependencyInfoDto(){}

    public DependencyInfoDto(String description, String result, String details) {
        this.description = description;
        this.result = result;
        this.details = details;
    }

    public static DependencyInfoDto of(Map<String, Object> details){
        ModelMapper mapper = new ModelMapper();
        DependencyInfoDto dependencyInfoDto = new DependencyInfoDto();
        mapper.map(details, dependencyInfoDto);

        return dependencyInfoDto;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
