package gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gent.digipolis.servicefactory.module.healtcheck.container.configuration.ServiceInfoConfig;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.Dependency;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Brecht
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AboutDto {
    private String id;
    private String name;
    private String description;
    private String protocol;
    private List<String> owners;
    private String version;
    private String host;
    private String projectRepo;
    private String projectHome;
    private List<String> logsLinks;
    private List<String> statsLinks;
    private List<DependencyDto> dependencies;
    private Object customData;

    private AboutDto() {
        this.owners = new ArrayList<>();
        this.logsLinks = new ArrayList<>();
        this.statsLinks = new ArrayList<>();
        this.dependencies = new ArrayList<>();
    }

    public static AboutDto of(ServiceInfoConfig serviceInfoConfig, List<Dependency> dependencies){
        AboutDto aboutDto = new AboutDto();
        ModelMapper mapper = new ModelMapper();
        mapper.map(serviceInfoConfig, aboutDto);
        aboutDto.setDependencies(dependencies.stream()
                .map(DependencyDto::of)
                .collect(Collectors.toList()));
        return aboutDto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public List<String> getOwners() {
        return owners;
    }

    public void setOwners(List<String> owners) {
        this.owners = owners;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProjectRepo() {
        return projectRepo;
    }

    public void setProjectRepo(String projectRepo) {
        this.projectRepo = projectRepo;
    }

    public String getProjectHome() {
        return projectHome;
    }

    public void setProjectHome(String projectHome) {
        this.projectHome = projectHome;
    }

    public List<String> getLogsLinks() {
        return logsLinks;
    }

    public void setLogsLinks(List<String> logsLinks) {
        this.logsLinks = logsLinks;
    }

    public List<String> getStatsLinks() {
        return statsLinks;
    }

    public void setStatsLinks(List<String> statsLinks) {
        this.statsLinks = statsLinks;
    }

    public List<DependencyDto> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<DependencyDto> dependencies) {
        this.dependencies = dependencies;
    }

    public Object getCustomData() {
        return customData;
    }

    public void setCustomData(Object customData) {
        this.customData = customData;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
