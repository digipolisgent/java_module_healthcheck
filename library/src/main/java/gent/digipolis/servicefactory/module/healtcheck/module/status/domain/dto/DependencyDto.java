package gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.Dependency;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brecht
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DependencyDto {
    private String name;
    private List<Object> status;
    private long statusDuration;
    private String statusPath;
    private String type;
    @JsonProperty("isTraversable")
    private boolean traversable;

    private DependencyDto() {
        this.traversable = false;
        this.type = "internal";
        this.status = new ArrayList<>();
    }

    public static DependencyDto of(Dependency dependency){
        DependencyDto dependencyDto = new DependencyDto();

        ModelMapper mapper = new ModelMapper();
        mapper.map(dependency, dependencyDto);

        dependencyDto.setName(dependency.getStatusPath());
        dependencyDto.getStatus().add(dependency.getStatus().toString());

        if(!dependency.getStatus().equals(Dependency.Status.OK)){
            dependencyDto.getStatus().add(dependency.getDependencyInfoDto());
        }

        return dependencyDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getStatus() {
        return status;
    }

    public void setStatus(List<Object> status) {
        this.status = status;
    }

    public long getStatusDuration() {
        return statusDuration;
    }

    public void setStatusDuration(long statusDuration) {
        this.statusDuration = statusDuration;
    }

    public String getStatusPath() {
        return statusPath;
    }

    public void setStatusPath(String statusPath) {
        this.statusPath = statusPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isTraversable() {
        return traversable;
    }

    public void setTraversable(boolean traversable) {
        this.traversable = traversable;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
