package gent.digipolis.servicefactory.module.healtcheck.container.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 * @author Brecht
 */
@Component
@ConfigurationProperties(prefix = "service-info")
public class ServiceInfoConfig {
    private String id;
    private String name;
    private String description;
    private String protocol;
    private List<String> owners;
    private String version;
    private String host;
    private String projectRepo;
    private String projectHome;
    private List<String> logsLinks;
    private List<String> statsLinks;

    protected ServiceInfoConfig() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public List<String> getOwners() {
        return owners;
    }

    public void setOwners(List<String> owners) {
        this.owners = owners;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProjectRepo() {
        return projectRepo;
    }

    public void setProjectRepo(String projectRepo) {
        this.projectRepo = projectRepo;
    }

    public String getProjectHome() {
        return projectHome;
    }

    public void setProjectHome(String projectHome) {
        this.projectHome = projectHome;
    }

    public List<String> getLogsLinks() {
        return logsLinks;
    }

    public void setLogsLinks(List<String> logsLinks) {
        this.logsLinks = logsLinks;
    }

    public List<String> getStatsLinks() {
        return statsLinks;
    }

    public void setStatsLinks(List<String> statsLinks) {
        this.statsLinks = statsLinks;
    }

}