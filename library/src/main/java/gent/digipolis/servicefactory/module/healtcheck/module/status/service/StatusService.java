package gent.digipolis.servicefactory.module.healtcheck.module.status.service;

import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.AboutDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.DependencyDto;

import java.util.List;

/**
 * Created by jens on 16.11.17.
 */
public interface StatusService {
    AboutDto getAbout();
    List<Object> getAggregate();
    DependencyDto getDependency(String path);
}
