package gent.digipolis.servicefactory.module.healtcheck.module.status.domain;

/**
 * Created by jens on 16.11.17.
 */
public class HealthDetail {
    public static final String STATUS = "status";
    public static final String STATUS_DURATION = "statusDuration";
    public static final String DESCRIPTION = "description";
    public static final String RESULT = "result";
    public static final String DETAILS = "details";

    private HealthDetail(){}
}
