package gent.digipolis.servicefactory.module.healtcheck.module.status.service.impl;

import gent.digipolis.servicefactory.module.healtcheck.container.configuration.ServiceInfoConfig;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.Dependency;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.HealthDetail;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.AboutDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.DependencyDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.DependencyInfoDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.HealthEndpoint;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 *
 * @author Brecht
 */
@Service
public class StatusServiceImpl implements StatusService {
    private final HealthEndpoint healthEndpoint;
    private final ServiceInfoConfig serviceInfoConfig;

    @Autowired
    public StatusServiceImpl(HealthEndpoint healthEndpoint, ServiceInfoConfig serviceInfoConfig){
        this.healthEndpoint = healthEndpoint;
        this.serviceInfoConfig = serviceInfoConfig;
    }

    @Override
    public AboutDto getAbout() {
        return AboutDto.of(serviceInfoConfig, fetchDependencies());
    }

    @Override
    public List<Object> getAggregate() {
        List<Dependency> dependencies = fetchDependencies();
        List<Object> aggregate = new ArrayList<>();

        // Find Status with highest order
        String status = dependencies.stream()
                .map(Dependency::getStatus)
                .max(Comparator.comparing(Dependency.Status::getOrder))
                .orElse(Dependency.Status.OK)
                .getValue();

        if (status != null) {
            aggregate.add(status);
        }

        // Concatenate descriptions
        String descriptions = dependencies.stream()
                .filter(dependency -> !dependency.getStatus().equals(Dependency.Status.OK))
                .map(dependency -> dependency.getDependencyInfoDto().getDescription())
                .collect(Collectors.joining("; "));

        // Concatenate details
        String details = dependencies.stream()
                .filter(dependency -> !dependency.getStatus().equals(Dependency.Status.OK))
                .map(dependency -> dependency.getDependencyInfoDto().getDetails())
                .collect(Collectors.joining("; "));

        // Only add the info overview if the status is CRIT or WARN
        if (status != null && (status.equals(Dependency.Status.CRIT.getValue()) || status.equals(Dependency.Status.WARN.getValue()))) {
            DependencyInfoDto totalInfo = new DependencyInfoDto(descriptions, status, details);
            aggregate.add(totalInfo);
        }

        return aggregate;
    }

    @Override
    public DependencyDto getDependency(String path) {
        return fetchDependencies().stream()
                .filter(dependency -> dependency.getStatusPath().equalsIgnoreCase(path))
                .map(DependencyDto::of)
                .findAny().orElse(null);
    }

    private List<Dependency> fetchDependencies() {
        Health health = healthEndpoint.invoke();
        Map<String, Object> detailMap = health.getDetails();

        return detailMap.entrySet().stream()
                .filter(StatusServiceImpl::isHealthDetailsEntry)
                .map(StatusServiceImpl::toDependency)
                .collect(Collectors.toList());
    }

    private static boolean isHealthDetailsEntry(Entry<String, Object> entry){
        Health health = (Health) entry.getValue();
        return health.getDetails().containsKey(HealthDetail.DESCRIPTION);
    }

    private static Dependency toDependency(Entry<String, Object> entry){
        Health health = (Health) entry.getValue();
        return Dependency.of(entry.getKey(), health.getDetails());
    }
}