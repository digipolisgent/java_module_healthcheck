package gent.digipolis.servicefactory.module.healtcheck.module.status.rest;

import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.AboutDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.DependencyDto;
import gent.digipolis.servicefactory.module.healtcheck.module.status.service.StatusService;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * https://hootsuite.github.io/health-checks-api/
 *
 * @author Brecht
 */
@RestController
@RequestMapping(value = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatusController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final StatusService statusService;

    @Autowired
    public StatusController(StatusService statusService){
        this.statusService = statusService;
    }

    /**
     * https://hootsuite.github.io/health-checks-api/#status-am-i-up-get
     *
     * Status endpoints used to check the health of a service.
     *
     * @return Content-Type: application/text "OK"
     */
    @RequestMapping(method = RequestMethod.GET, value = "/am-i-up", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> amIUp() {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    /**
     * https://hootsuite.github.io/health-checks-api/#status-aggregate-get
     *
     * Checks the overall status of the service by checking the status of
     * dependencies. By default, this endpoint checks all dependencies or can be
     * filtered to check only internal or external dependencies.
     *
     * @param type string (optional) Example: "internal" The type of
     * dependencies to check (internal, external).
     *
     * @return Content-Type: application/json
     */
    @RequestMapping(method = RequestMethod.GET, value = "/aggregate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Object>> aggregate(@RequestParam(required = false, value = "type", defaultValue = "all") String type) {
        List<Object> response = this.statusService.getAggregate();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * https://hootsuite.github.io/health-checks-api/#status-about-get
     *
     * Gets metadata about the service including it’s dependencies and their
     * current status
     *
     * @return Content-Type: application/json
     */
    @RequestMapping(method = RequestMethod.GET, value = "/about", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> about() {
        AboutDto aboutDto = this.statusService.getAbout();
        return new ResponseEntity<>(aboutDto, HttpStatus.OK);
    }

    /**
     * https://hootsuite.github.io/health-checks-api/#status-traverse-get
     *
     * Traverse to another node in the graph and run an “action” at that level
     * and return the result. The default action is about.
     *
     * @param dependencies string (optional) Default: `` Example:
     * service1,service2 A comma delimited list of services to traverse.
     * @param action string (optional) Default: about Example: about The action
     * to run at the last node in the traversal.
     *
     * @return Content-Type: application/json
     */
    @RequestMapping(method = RequestMethod.GET, value = "/traverse", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> traverse(@RequestParam(required = false) String dependencies, @RequestParam(required = false) String action) {
        log.warn("This method is not implemented. None of the dependencies in our use-case were traversable.");
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * https://hootsuite.github.io/health-checks-api/#status-dependency-get
     *
     * Gets the status of a configured dependency using its statusPath. There
     * are 3 levels of status: OK - Everything is all good WARN - The dependency
     * is in a sub optimal state. See details in response for more info. CRIT -
     * The dependency has an error. See details in response for more info.
     *
     * @param dependency string (required) Example: "mysql-db" The statusPath of
     * configured dependency to check the status of.
     *
     * @return Content-Type: application/json
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{dependency}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> dependency(@PathVariable @NotEmpty String dependency) {
        DependencyDto dependencyDtoByPath = this.statusService.getDependency(dependency);
        return new ResponseEntity<>(dependencyDtoByPath.getStatus(), HttpStatus.OK);
    }
}
