package gent.digipolis.servicefactory.module.healtcheck.module.status.domain;

import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.dto.DependencyInfoDto;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Map;

/**
 *
 * @author Brecht
 */
public class Dependency {
    private Status status;
    private DependencyInfoDto dependencyInfoDto;
    private long statusDuration;
    private String statusPath;

    private Dependency() {
    }

    public static Dependency of(String path, Map<String, Object> details){
        Dependency dependency = new Dependency();

        dependency.setStatus((Dependency.Status) details.get(HealthDetail.STATUS));
        dependency.setDependencyInfoDto(DependencyInfoDto.of(details));
        dependency.setStatusPath(path);
        dependency.setStatusDuration((long) details.get(HealthDetail.STATUS_DURATION));

        return dependency;
    }

    public long getStatusDuration() {
        return statusDuration;
    }

    public void setStatusDuration(long statusDuration) {
        this.statusDuration = statusDuration;
    }

    public String getStatusPath() {
        return statusPath;
    }

    public void setStatusPath(String statusPath) {
        this.statusPath = statusPath;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public enum Status {
        OK("OK", 1),
        WARN("WARN", 2),
        CRIT("CRIT", 3);

        private String value;
        private int order;

        Status(String value, int order) {
            this.value = value;
            this.order = order;
        }

        public int getOrder() {
            return this.order;
        }

        public String getValue() {
            return value;
        }
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DependencyInfoDto getDependencyInfoDto() {
        return dependencyInfoDto;
    }

    public void setDependencyInfoDto(DependencyInfoDto dependencyInfoDto) {
        this.dependencyInfoDto = dependencyInfoDto;
    }
}
