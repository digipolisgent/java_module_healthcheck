package gent.digipolis.servicefactory.example.container;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Be sure to include the correct packages in the scanBasePackages
 * the dependency packages first, then the native package
 * @author Brecht
 */
@SpringBootApplication(scanBasePackages = {"gent.digipolis.servicefactory"})
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}