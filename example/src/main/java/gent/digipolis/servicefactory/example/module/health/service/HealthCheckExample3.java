package gent.digipolis.servicefactory.example.module.health.service;

import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.Dependency;
import gent.digipolis.servicefactory.module.healtcheck.module.status.domain.HealthDetail;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html#_writing_custom_healthindicators
 *
 * Example of how to extend the health check to pass dependency information into
 * the hootsuite-actuator rewrite
 *
 * @author Brecht
 */
@Component
public class HealthCheckExample3 implements HealthIndicator {

    @Override
    public Health health() {
        // Do your health check here
        // ...

        // Return health status
        return Health.up()
                .withDetail(HealthDetail.DESCRIPTION, "should be read in aggregate!")
                .withDetail(HealthDetail.RESULT, Dependency.Status.OK)
                .withDetail(HealthDetail.STATUS, Dependency.Status.OK)
                .withDetail(HealthDetail.DETAILS, "should be read in aggregate!")
                .withDetail(HealthDetail.STATUS_DURATION, (long) 0)
                .build();
    }
}
